document.getElementById("Verif").addEventListener("click", SysErr);

function SysErr() {
    document.getElementById('Erreur-espace').style.display = "none";
    document.getElementById('Erreur-Special').style.display = "none";
    document.getElementById('Erreur-Chiffre').style.display = "none";
    document.getElementById('Erreur-Vide').style.display = "none";
    document.getElementById('Erreur-message').innerHTML = '';

    var username = document.getElementById("UserName");
    var MDP = document.getElementById('MDP');

    function containsSpecialChars(str) {
        const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
        return specialChars.test(str);
    }

    function containsSpace(str) {
        const specialChars = /[ ]/;
        return specialChars.test(str);
    }


    if (username.value.length == 0) {
        document.getElementById('Erreur-Vide').style.display = "block";
    } else if (containsSpecialChars(username.value)) {
        document.getElementById('Erreur-Special').style.display = "block";
    } else if (containsSpace(username.value)) {
        document.getElementById('Erreur-espace').style.display = "block";
    } else if (MDP.value.length == 0) {
        document.getElementById('Erreur-message').innerHTML = "Veuillez remplir ce champ : Mot de passe";
    }
}